package com.bivre.android_viewpager;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ページャー
        FragmentManager manager = getSupportFragmentManager();
        final ViewPager viewPager = (ViewPager)findViewById(R.id.viewPager);
        MainFragmentPagerAdapter adapter = new MainFragmentPagerAdapter(manager);
        viewPager.setAdapter(adapter);

        // タブ
        final TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();
        for (int i = 0; i < adapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTabSpec(String.valueOf(i)).setIndicator(adapter.getPageTitle(i)).setContent(android.R.id.tabcontent));
        }

        // イベント
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                viewPager.setCurrentItem(Integer.valueOf(tabId));
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabHost.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
